Drupal.pointboard = Drupal.pointboard || {};

Drupal.pointboard.addItem = function(data) {
  jQuery('#point-board-add #edit-order').val(data['data']['order']);
  $('#dialog').dialog('open');
}

Drupal.behaviors.pointboardInit = function() {
  jQuery.each(jQuery('div#point-board-grid div.point-board-item'), function(i, obj) {
  	if($(obj).hasClass() != 'point-board-item-free')
      jQuery(obj).bind('click', {'order': i}, Drupal.pointboard.addItem);
  })

  var name = $("#edit-name"),
      url = $("#edit-url"),
      file = $("#edit-image"),
      allFields = $([]).add(name).add(url).add(file),
      tips = $("#validateTips");

  function updateTips(t) {
    tips.text(Drupal.t(t)).effect("highlight", {}, 1500);
  }

  function checkLength(o, n, min, max) {
    if ( o.val().length > max || o.val().length < min ) {
      o.addClass('ui-state-error');
      updateTips(Drupal.t("Length of @name must be between @min and @max.", {'@name': n, '@min': min, '@max': max}));
      return false;
    } else {
      return true;
    }
  }

  function checkRegexp(o, regexp, n) {
    if ( !( regexp.test( o.val() ) ) ) {
      o.addClass('ui-state-error');
      updateTips(n);
      return false;
    } else {
      return true;
    }
  }

  $("#dialog").dialog({
    autoOpen: false,
    height: 300,
    width: 300,
    modal: true,
    buttons: {
      'Submit': function() {
        var bValid = true;
        allFields.removeClass('ui-state-error');

        bValid = bValid && checkLength(name, "Name", 3, 255);
        bValid = bValid && checkRegexp(name, /^[a-z]([0-9a-z_\s])+$/i, Drupal.t("Username may consist of space, a-z, 0-9, underscores, begin with a letter."));

        bValid = bValid && checkLength(url, "URL", 5, 255);
        bValid = bValid && checkRegexp(file, /[\.jpg\.jpeg\.png\.gif]$/i, Drupal.t("You have to browse: jpg, jpeg, png or gif picture."));

        if (bValid) {
		      jQuery('#point-board-add').submit();
          $(this).dialog('close');
        }
      },
      'Cancel': function() {
        $(this).dialog('close');
      }
    },
    close: function() {
      allFields.val('').removeClass('ui-state-error');
    }
  });
}

