Point Board
===========

It is a simple module to get ability for your visitors leave
a link to their site with picture. Also your can collect all
your best links with picture in one block - Point Board.


Future Updates
==============

1. WebMoney, PayPal and other payment systems back to get
   your visitors payment for their links

2. More flexible settings to adjust modal window into your
   Drupal site design
